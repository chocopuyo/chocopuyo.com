//視差効果で使うクラス
function Parallax(){}
Parallax.prototype.add = function(start_top,id,h_val,img_arr,href_arr ){
  var wh = $(window).height();
  var tmp_val = 1.3-(h_val-start_top)/(1.5*wh)
  $(id+" .text").css({"position":"fixed",top:"0px",width:"100%"})
  $(".circle div").css({opacity:tmp_val})
  if(img_arr){
    (img_arr[0])?$('#first-circle').html("<a href='"+href_arr[0]+"'><img src='./images/"+img_arr[0]+"' /></a>"):$('#first-circle').text("・ω・");
    (img_arr[1])?$('#second-circle').html("<a href='"+href_arr[1]+"'><img src='./images/"+img_arr[1]+"' /></a>"):$('#second-circle').text("・ω・");
    (img_arr[2])?$('#third-circle').html("<a href='"+href_arr[2]+"'><img src='./images/"+img_arr[2]+"' /></a>"):$('#third-circle').text("・ω・");
  }
}
Parallax.prototype.clear = function(id){
  $(id+" .text").css({"position":"relative"})
}

$(function(){
  var para = new Parallax()
  var tmp_val = 0.0
  var wh = $(window).height();
 $(window).scroll(function() {
   var h_val = $(this).scrollTop(); //スクロール値を取得
   //$("#debug").text(tmp_val)
   //$("#debug").text($(".content").height())
   //各セクションの固定
   if(h_val>0 && h_val<=wh){
    tmp_val = 1.3-(h_val)/(wh/2)/(1.5*wh)
    $(".circle div").css({opacity:tmp_val})
    $('#first-circle').text("・ω・")
    $('#second-circle').text("・ω・")
    $('#third-circle').text("・ω・")
   }

   if(h_val>wh/2 && h_val<=wh*2){
     para.add(wh/2,"#first",h_val,["snowwhite.png","tsukubagame.png","chocopla.png"],["http://3under-b-step.sakura.ne.jp/snow_white/","http://www.youtube.com/watch?v=UwQOY3jUH-4","http://chocopuyo.com/planet"])
   }else{
     para.clear("#first")
   }
   if(h_val>wh*2 && h_val<=wh*3.5){
     para.add(wh*2,"#second",h_val,["urasunday.png","chinanago.png","azupero.png"],["http://urasunday.chocopuyo.com","http://chocopuyo.com/chinanago","http://www.u.tsukuba.ac.jp/~s1321645/azunyanperopero/"])
   }else{
     para.clear("#second")
   }
   if(h_val>wh*3.5 && h_val<=wh*5){
     para.add(wh*3.5,"#third",h_val,["liclee.png","sweets.png","eco.png"],["http://aktsk.jp/press/646/","http://gamebiz.jp/?p=384",""])
   }else{
     para.clear("#third")
   }
   if(h_val>wh*5 && h_val<=wh*6.5){
     para.add(wh*5,"#forth",h_val,["twitter.png","facebook.png","blogger.png"],["http://twitter.com/chocopuyo","http://facebook.com/chocopuyo","http://blog.chocopuyo.com"])
   }else{
     para.clear("#forth")
   }


 });
})

